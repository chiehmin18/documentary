## Env Variables

All environment variables will be inherited from the _Documentary_ process. Additional variables can be passed in the `env` argument, which is only available for the `<fork>` component and not the `%FORK%` marker. The variables will be split by whitespace, and the key-value pairs will be split by `=`. It's not possible to specify values with `=` in them at the moment.

Same forks but with different env variables are cached separately.

%~%
