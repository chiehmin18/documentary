## Stderr

By default, the `FORK` marker will print the `stdout` output. To print the `stderr` output, there is the `FORKERR` marker.

```t
%FORKERR(-lang)? module ...args%
```

It works exactly the same as `%FORK%` but will print the output of the process's `stderr` stream.


<table>
<thead>
 <tr>
  <th>Markdown</th><th>JavaScript</th>
 </tr>
</thead>
<tbody>
 <tr/>
 <tr>
  <td>

%EXAMPLE: example/fork/fork-stderr.md%
  </td>

  <td>

%EXAMPLE: example/fork/fork-stderr%
  </td>
 </tr>
 <tr>
 <td colspan="2" align="center"><strong>Output<strong></td>
 </tr>
 <tr>
 <td colspan="2">

%FORK-md src/bin/doc example/fork/fork-stderr.md%
 </td>
 </tr>
</table>

%~%