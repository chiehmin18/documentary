## `<fork>`

The `<fork>` component is the same as the `%FORK%` marker, but it offers extended functionality. The optional properties described above on this page are specified in the arguments, and the location of the fork module is passed as a child.

```jsx
<fork nocache plain relative stderr lang="js"
  env="ENV_VAR=testing SECRET_KEY=hello-world">
  module/location/run.js
</fork>
```

%~%