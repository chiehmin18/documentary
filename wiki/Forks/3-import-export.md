## Import/Exports Support

_Documentary_ is able to fork modules that use `import` and `export` without the developer having to write a proxy file that would otherwise require `@babel/register` or other runtime transpilers. It was made possible with a simple [_ÀLaMode_](https://github.com/a-la/alamode) regex-based transpiler that will update the `import/export` statements on-the-fly. If there are any problems while using this feature, it can be disabled with the _plain_ `_` symbol: `%_FORK module arg1 arg2%`.

%~%