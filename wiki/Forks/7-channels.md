## Both Channels

Whenever there are two forks of the same module with the same arguments and env variables, but one prints `stdout` and another one `stderr`, only one fork will be spawn, and its result reused. That saves time when trying to document the total output of the program, possibly in different sections, for example in a 2-column table:

```markdown
<table>
<tr><th>STDOUT</th><th>STDERR</th></tr>
<!-- block-start -->
<tr><td>

<fork>example</fork>
</td>
<td>

<fork>example</fork>
</td></tr>
</table>
```

%~%
