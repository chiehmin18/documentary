## Caching

The output of forks will be cached in the `.documentary/cache` directory. When compiling documentation, _Documentary_ will check for the presence of cache, check the _mtime_ of the module and if it is the same as cached, analyse module's dependencies to see if any of them had changes (updates to package dependencies' versions, changes to source files).

When the cache is matched, no forking will take place and the value will be taken from the saved outputs. To explicitly prevent caching on a particular _FORK_ marker, it should be prefixed with `!`: `%!FORK module arg1 arg2%`. To disable caching across all _forks_, the [`-c`](CLI#disable-cache) option can be passed to the CLI. In this case, even though caching was disabled, the new output will still be written to cache so that when _Documentary_ is run next time, the latest known output is placed instantly.

%~%
