When placing examples, it is important to show the output that they produce. This can be achieved using the `FORK` marker.

```t
%[/!_]FORK(-lang)? module ...args%
```

Listing [[examples|Embed-Examples]] and forking source code in documentation, gives another layer of **Quality Assurance** of the program, since after the compilation of the README file, any errors due to introduced changes to the source code will appear there. In fact, in some cases, documentation-driven development can even substitute tests.

## On This Page

%TOC%

%~%

The `%FORK%` marker will make _Documentary_ fork a _Node.JS_ module using the `child_process.fork` function. The output is printed in a code block, with optionally given language. If the process cleared lines with `\r`, the output will be adjusted to account for that to be displayed like it would be in the terminal. The environment variables will be inherited from the parent `doc` process.

<table>
<thead>
 <tr>
  <th>Markdown</th><th>JavaScript</th>
 </tr>
</thead>
<tbody>
 <tr/>
 <tr>
  <td>

%EXAMPLE: example/fork/fork.md%
  </td>

  <td>

%EXAMPLE: example/fork/fork%
  </td>
 </tr>
 <tr>
 <td colspan="2" align="center"><strong>Output<strong></td>
 </tr>
 <tr>
 <td colspan="2">

%FORK src/bin/doc example/fork/fork.md%
 </td>
 </tr>
</table>

<!-- %FORK example example/fork% -->

%~%

