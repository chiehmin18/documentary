## Replace Absolute Paths

With the <kbd>/</kbd> prefix in the FORK command, all absolute paths that contain the current working directory, will be replaced with relative paths.

```
%/FORKERR-table example/print-table%
```

This can help with documenting errors or other code that prints absolute paths in a nimble way.

%/_FORKERR-js example/fork/absolute%

%~%