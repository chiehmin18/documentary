const { EOL } = require('os')
const text = JSON.stringify({
  hello: 'world',
}, null, 2).replace(/\n/g, EOL)
console.log(text)
