import { Readable } from 'stream'
import Zoroaster from 'zoroaster'
import Context, { MarkdownSnapshot } from '../context'
import Toc, { getToc } from '../../src/lib/Toc'
import { getStream } from '../../src/lib'

/** @type {Object.<string, (c: Context, z:Zoroaster)>} */
const T = {
  context: [Context, Zoroaster, MarkdownSnapshot],
  async'reads TOC'({ README_PATH, Documentary }) {
    const documentary = new Documentary({ noCache: true })
    const toc = new Toc({ documentary })
    const rs = getStream(README_PATH)
    return rs.pipe(documentary).pipe(toc)
  },
  async'underlined'({ DocumentaryNoDToc }) {
    const stream = new Readable({
      read() {
        this.push(` \`test\`
test2
-----
test3
-----`)
        this.push(null)
      },
    })
    stream.pipe(DocumentaryNoDToc)
    const toc = await getToc(DocumentaryNoDToc)
    return toc
  },
  async'underlined with CR'({ DocumentaryNoDToc }, { snapshotSource }) {
    snapshotSource('underlined')
    const stream = new Readable({
      read() {
        this.push(` \`test\`\r
test2\r
-----\r
test3\r
-----`)
        this.push(null)
      },
    })
    stream.pipe(DocumentaryNoDToc)
    const toc = await getToc(DocumentaryNoDToc)
    return toc
  },
}

export default T