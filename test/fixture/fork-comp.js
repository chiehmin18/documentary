import askQuestions from 'reloquent'
import { createInterface } from 'readline'
import { EOL } from 'os'
import { inspect } from 'util'

const log = (s) => process.stdout.write(`${typeof s == 'string' ? s : inspect(s)}${EOL}`)

;(async () => {
  const res = await askQuestions({
    'a': 'Are you sure?',
    'b': 'Please confirm',
  })
  log(res)
  const i = createInterface({
    input: process.stdin,
    output: process.stderr,
  })
  i.question('STDERR question: ', (answer) => {
    log(answer)
    i.close()
    process.exit()
  })
})()