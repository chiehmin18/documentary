{ wiki: '../documentary.wiki' }

## replaces a section break
%~%

/* expected */
<div align="center"><a href="#table-of-contents">
  [[/.documentary/section-breaks/0.svg]]
</a></div>
/**/

## replaces multiple section breaks
%~%

%~%

/* expected */
<div align="center"><a href="#table-of-contents">
  [[/.documentary/section-breaks/0.svg]]
</a></div>

<div align="center"><a href="#table-of-contents">
  [[/.documentary/section-breaks/1.svg]]
</a></div>
/**/

## replaces multiple section breaks when 22 is reached
%~ 22%

%~%

/* expected */
<div align="center"><a href="#table-of-contents">
  [[/.documentary/section-breaks/22.svg]]
</a></div>

<div align="center"><a href="#table-of-contents">
  [[/.documentary/section-breaks/0.svg]]
</a></div>
/**/

## replaces section break with a href attribute
%~ href="#test"%

/* expected */
<div align="center"><a href="#test">
  [[/.documentary/section-breaks/0.svg]]
</a></div>
/**/

## replaces section break with a width attribute
%~ width="200"%

/* expected */
<div align="center"><a href="#table-of-contents">
  [[/.documentary/section-breaks/0.svg|width=200]]
</a></div>
/**/

## replaces section break with a negative number
%~ -1%

/* expected */
<div align="center"><a href="#table-of-contents">
  [[/.documentary/section-breaks/-1.svg]]
</a></div>
/**/

## replaces multiple section break when one is negative
%~%
%~ -1%
%~%

/* expected */
<div align="center"><a href="#table-of-contents">
  [[/.documentary/section-breaks/0.svg]]
</a></div>
<div align="center"><a href="#table-of-contents">
  [[/.documentary/section-breaks/-1.svg]]
</a></div>
<div align="center"><a href="#table-of-contents">
  [[/.documentary/section-breaks/1.svg]]
</a></div>
/**/

## sets a destination
%~ to=".docs/breaks"%

/* expected */
<div align="center"><a href="#table-of-contents">
  [[/.docs/breaks/0.svg]]
</a></div>
/**/

## replaces a section break with multiple attributes
%~ width="25" href="#top"%

/* expected */
<div align="center"><a href="#top">
  [[/.documentary/section-breaks/0.svg|width=25]]
</a></div>
/**/