## replaces a section brake
%~%

/* expected */
<div align="center"><a href="#table-of-contents">
  <img src="/.documentary/section-breaks/0.svg?sanitize=true">
</a></div>
/**/

## replaces multiple section brakes
%~%

%~%

/* expected */
<div align="center"><a href="#table-of-contents">
  <img src="/.documentary/section-breaks/0.svg?sanitize=true">
</a></div>

<div align="center"><a href="#table-of-contents">
  <img src="/.documentary/section-breaks/1.svg?sanitize=true">
</a></div>
/**/

## replaces multiple section brakes when 22 is reached
%~ 22%

%~%

/* expected */
<div align="center"><a href="#table-of-contents">
  <img src="/.documentary/section-breaks/22.svg?sanitize=true">
</a></div>

<div align="center"><a href="#table-of-contents">
  <img src="/.documentary/section-breaks/0.svg?sanitize=true">
</a></div>
/**/

## replaces section brake with a href attribute
%~ href="#test"%

/* expected */
<div align="center"><a href="#test">
  <img src="/.documentary/section-breaks/0.svg?sanitize=true">
</a></div>
/**/

## replaces section brake with a width attribute
%~ width="200"%

/* expected */
<div align="center"><a href="#table-of-contents">
  <img src="/.documentary/section-breaks/0.svg?sanitize=true" width="200">
</a></div>
/**/

## replaces section brake with a negative number
%~ -1%

/* expected */
<div align="center"><a href="#table-of-contents">
  <img src="/.documentary/section-breaks/-1.svg?sanitize=true">
</a></div>
/**/

## replaces multiple section brake when one is negative
%~%
%~ -1%
%~%

/* expected */
<div align="center"><a href="#table-of-contents">
  <img src="/.documentary/section-breaks/0.svg?sanitize=true">
</a></div>
<div align="center"><a href="#table-of-contents">
  <img src="/.documentary/section-breaks/-1.svg?sanitize=true">
</a></div>
<div align="center"><a href="#table-of-contents">
  <img src="/.documentary/section-breaks/1.svg?sanitize=true">
</a></div>
/**/

## sets a destination
%~ to=".docs/breaks"%

/* expected */
<div align="center"><a href="#table-of-contents">
  <img src="/.docs/breaks/0.svg?sanitize=true">
</a></div>
/**/