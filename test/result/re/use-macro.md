## detects a single use
%USE-MACRO test
<data>hello</data>
<data>world</data>
%

/* matches */
[{"name": "test"}]
/**/

/* expected */
<data>hello</data>
<data>world</data>
/**/

## detects a multiple uses
%USE-MACRO test
<data>hello</data>
<data>world</data>
%

%USE-MACRO test2
<data>world</data>
<data>hello</data>
%

/* matches */
[{"name": "test"},
 {"name": "test2"}]
/**/

/* expected */
<data>hello</data>
<data>world</data>
<data>world</data>
<data>hello</data>
/**/