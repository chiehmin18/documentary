#!/usr/bin/env node
             
const fs = require('fs');
const os = require('os');
const stream = require('stream');
const path = require('path');
const tty = require('tty');
const util = require('util');
const _crypto = require('crypto');
const child_process = require('child_process');
const _module = require('module');             
const aa = fs.createReadStream, ba = fs.createWriteStream, ca = fs.lstat, da = fs.mkdir, fa = fs.mkdirSync, ha = fs.readdir, ja = fs.readlink, ka = fs.symlink;
const la = (a, b = 0, c = !1) => {
  if (0 === b && !c) {
    return a;
  }
  a = a.split("\n", c ? b + 1 : void 0);
  return c ? a[a.length - 1] : a.slice(b).join("\n");
}, ma = (a, b = !1) => la(a, 2 + (b ? 1 : 0)), na = a => {
  ({callee:{caller:a}} = a);
  return a;
};
const C = os.EOL, oa = os.homedir;
const pa = /\s+at.*(?:\(|\s)(.*)\)?/, qa = /^(?:(?:(?:node|(?:internal\/[\w/]*|.*node_modules\/(?:IGNORED_MODULES)\/.*)?\w+)\.js:\d+:\d+)|native)/, ra = a => {
  const {pretty:b = !1, ignoredModules:c = ["pirates", "@artdeco/pirates"]} = {}, d = c.join("|"), e = new RegExp(qa.source.replace("IGNORED_MODULES", d));
  return a.replace(/\\/g, "/").split("\n").filter(f => {
    f = f.match(pa);
    if (null === f || !f[1]) {
      return !0;
    }
    f = f[1];
    return f.includes(".app/Contents/Resources/electron.asar") || f.includes(".app/Contents/Resources/default_app.asar") ? !1 : !e.test(f);
  }).filter(f => f.trim()).map(f => {
    if (b) {
      const g = oa().replace(/\\/g, "/");
      return f.replace(/\s+at.*(?:\(|\s)(.*)\)?/, (k, l) => k.replace(l, l.replace(g, "~")));
    }
    return f;
  }).join("\n");
};
function sa(a, b, c = !1) {
  return function(d) {
    var e = na(arguments), {stack:f} = Error();
    const g = la(f, 2, !0), k = (f = d instanceof Error) ? d.message : d;
    e = [`Error: ${k}`, ...null !== e && a === e || c ? [b] : [g, b]].join("\n");
    e = ra(e);
    return Object.assign(f ? d : Error(), {message:k, stack:e});
  };
}
;function G(a) {
  var {stack:b} = Error();
  const c = na(arguments);
  b = ma(b, a);
  return sa(c, b, a);
}
;var ta = stream;
const ua = stream.PassThrough, va = stream.Transform, wa = stream.Writable;
const xa = (a, b) => {
  b.once("error", c => {
    a.emit("error", c);
  });
  return b;
};
class ya extends wa {
  constructor(a) {
    const {binary:b = !1, rs:c = null, ...d} = a || {}, {H:e = G(!0), proxyError:f} = a || {}, g = (k, l) => e(l);
    super(d);
    this.g = [];
    this.F = new Promise((k, l) => {
      this.on("finish", () => {
        let h;
        b ? h = Buffer.concat(this.g) : h = this.g.join("");
        k(h);
        this.g = [];
      });
      this.once("error", h => {
        if (-1 == h.stack.indexOf("\n")) {
          g`${h}`;
        } else {
          const m = ra(h.stack);
          h.stack = m;
          f && g`${h}`;
        }
        l(h);
      });
      c && xa(this, c).pipe(this);
    });
  }
  _write(a, b, c) {
    this.g.push(a);
    c();
  }
  get promise() {
    return this.F;
  }
}
const za = async(a, b = {}) => {
  ({promise:a} = new ya({rs:a, ...b, H:G(!0)}));
  return await a;
};
async function Aa(a) {
  a = aa(a);
  return await za(a);
}
;async function J(a, b, c) {
  const d = G(!0);
  if ("function" != typeof a) {
    throw Error("Function must be passed.");
  }
  if (!a.length) {
    throw Error(`Function${a.name ? ` ${a.name}` : ""} does not accept any arguments.`);
  }
  return await new Promise((e, f) => {
    const g = (l, h) => l ? (l = d(l), f(l)) : e(c || h);
    let k = [g];
    Array.isArray(b) ? k = [...b, g] : 1 < Array.from(arguments).length && (k = [b, g]);
    a(...k);
  });
}
;const Ba = path.basename, L = path.dirname, M = path.join, Ca = path.parse, Da = path.relative, Ea = path.resolve;
async function Fa(a) {
  const b = L(a);
  try {
    return await Ga(b), a;
  } catch (c) {
    if (/EEXIST/.test(c.message) && -1 != c.message.indexOf(b)) {
      return a;
    }
    throw c;
  }
}
async function Ga(a) {
  try {
    await J(da, a);
  } catch (b) {
    if ("ENOENT" == b.code) {
      const c = L(a);
      await Ga(c);
      await Ga(a);
    } else {
      if ("EEXIST" != b.code) {
        throw b;
      }
    }
  }
}
function Ha(a) {
  try {
    fa(a);
  } catch (b) {
    if ("ENOENT" == b.code) {
      const c = L(a);
      Ha(c);
      Ha(a);
    } else {
      if ("EEXIST" != b.code) {
        throw b;
      }
    }
  }
}
;async function Ia(a, b) {
  b = b.map(async c => {
    const d = M(a, c);
    return {lstat:await J(ca, d), path:d, relativePath:c};
  });
  return await Promise.all(b);
}
const Ja = a => a.lstat.isDirectory(), Ka = a => !a.lstat.isDirectory();
async function La(a, b = {}) {
  if (!a) {
    throw Error("Please specify a path to the directory");
  }
  const {ignore:c = []} = b;
  if (!(await J(ca, a)).isDirectory()) {
    throw b = Error("Path is not a directory"), b.code = "ENOTDIR", b;
  }
  b = await J(ha, a);
  var d = await Ia(a, b);
  b = d.filter(Ja);
  d = d.filter(Ka).reduce((e, f) => {
    var g = f.lstat.isDirectory() ? "Directory" : f.lstat.isFile() ? "File" : f.lstat.isSymbolicLink() ? "SymbolicLink" : void 0;
    return {...e, [f.relativePath]:{type:g}};
  }, {});
  b = await b.reduce(async(e, {path:f, relativePath:g}) => {
    const k = Da(a, f);
    if (c.includes(k)) {
      return e;
    }
    e = await e;
    f = await La(f);
    return {...e, [g]:f};
  }, {});
  return {content:{...d, ...b}, type:"Directory"};
}
;const Ma = async(a, b) => {
  const c = aa(a), d = ba(b);
  c.pipe(d);
  await Promise.all([new Promise((e, f) => {
    c.on("close", e).on("error", f);
  }), new Promise((e, f) => {
    d.on("close", e).on("error", f);
  })]);
}, Na = async(a, b) => {
  a = await J(ja, a);
  await J(ka, [a, b]);
}, Oa = async(a, b) => {
  await Fa(M(b, "path.file"));
  const {content:c} = await La(a), d = Object.keys(c).map(async e => {
    const {type:f} = c[e], g = M(a, e);
    e = M(b, e);
    "Directory" == f ? await Oa(g, e) : "File" == f ? await Ma(g, e) : "SymbolicLink" == f && await Na(g, e);
  });
  await Promise.all(d);
};
function Pa(a, b, c, d = !1) {
  const e = [];
  b.replace(a, (f, ...g) => {
    f = g[g.length - 2];
    f = d ? {position:f} : {};
    g = g.slice(0, g.length - 2).reduce((k, l, h) => {
      h = c[h];
      if (!h || void 0 === l) {
        return k;
      }
      k[h] = l;
      return k;
    }, f);
    e.push(g);
  });
  return e;
}
;const Qa = new RegExp(`${/([^\s>=/]+)/.source}(?:\\s*=\\s*${/(?:"([\s\S]*?)"|'([\s\S]*?)')/.source})?`, "g"), Ra = new RegExp(`(?:\\s+((?:${Qa.source}\\s*)*))`);
const Ta = (a, b) => {
  a = (Array.isArray(a) ? a : [a]).join("|");
  return Pa(new RegExp(`<(${a})${Ra.source}?(?:${/\s*\/>/.source}|${/>([\s\S]+?)?<\/\1>/.source})`, "g"), b, "t a v v1 v2 c".split(" ")).map(({t:c, a:d = "", c:e = ""}) => {
    d = d.replace(/\/$/, "").trim();
    d = Sa(d);
    return {content:e, props:d, tag:c};
  });
}, Sa = a => Pa(Qa, a, ["key", "val", "def", "f"]).reduce((b, {key:c, val:d}) => {
  if (void 0 === d) {
    return b[c] = !0, b;
  }
  b[c] = "true" == d ? !0 : "false" == d ? !1 : /^\d+$/.test(d) ? parseInt(d, 10) : d;
  return b;
}, {});
const Ua = (a, b, c, d = !1, e = !1) => {
  const f = c ? new RegExp(`^-(${c}|-${b})$`) : new RegExp(`^--${b}$`);
  b = a.findIndex(g => f.test(g));
  if (-1 == b) {
    return {argv:a};
  }
  if (d) {
    return {value:!0, index:b, length:1};
  }
  d = a[b + 1];
  if (!d || "string" == typeof d && d.startsWith("--")) {
    return {argv:a};
  }
  e && (d = parseInt(d, 10));
  return {value:d, index:b, length:2};
}, Va = a => {
  const b = [];
  for (let c = 0; c < a.length; c++) {
    const d = a[c];
    if (d.startsWith("-")) {
      break;
    }
    b.push(d);
  }
  return b;
};
const Wa = /acit|ex(?:s|g|n|p|$)|rph|ows|mnc|ntw|ine[ch]|zoo|^ord/i;
let Xa = a => `${a}`.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;"), Ya = a => 40 < `${a}`.length || -1 != `${a}`.indexOf("\n") || -1 !== `${a}`.indexOf("<");
const Za = {};
function $a(a) {
  const b = {...a.attributes, children:a.children};
  a = a.nodeName.defaultProps;
  if (void 0 !== a) {
    for (let c in a) {
      void 0 === b[c] && (b[c] = a[c]);
    }
  }
  return b;
}
;const ab = (a, b, {allAttributes:c, xml:d, J:e, sort:f, w:g} = {}) => {
  let k;
  const l = Object.keys(a);
  f && l.sort();
  return {K:l.map(h => {
    var m = a[h];
    if ("children" != h && !h.match(/[\s\n\\/='"\0<>]/) && (c || !["key", "ref"].includes(h))) {
      if ("className" == h) {
        if (a.class) {
          return;
        }
        h = "class";
      } else {
        if ("htmlFor" == h) {
          if (a.for) {
            return;
          }
          h = "for";
        } else {
          if ("srcSet" == h) {
            if (a.srcset) {
              return;
            }
            h = "srcset";
          }
        }
      }
      e && h.match(/^xlink:?./) && (h = h.toLowerCase().replace(/^xlink:?/, "xlink:"));
      if ("style" == h && m && "object" == typeof m) {
        {
          let n = "";
          for (var p in m) {
            let q = m[p];
            null != q && (n && (n += " "), n += Za[p] || (Za[p] = p.replace(/([A-Z])/g, "-$1").toLowerCase()), n += ": ", n += q, "number" == typeof q && !1 === Wa.test(p) && (n += "px"), n += ";");
          }
          m = n || void 0;
        }
      }
      if ("dangerouslySetInnerHTML" == h) {
        k = m && m.__html;
      } else {
        if ((m || 0 === m || "" === m) && "function" != typeof m) {
          if (!0 === m || "" === m) {
            if (m = h, !d) {
              return h;
            }
          }
          p = "";
          if ("value" == h) {
            if ("select" == b) {
              g = m;
              return;
            }
            "option" == b && g == m && (p = "selected ");
          }
          return `${p}${h}="${Xa(m)}"`;
        }
      }
    }
  }).filter(Boolean), I:k, w:g};
};
const bb = [], cb = /^(area|base|br|col|embed|hr|img|input|link|meta|param|source|track|wbr)$/, db = /^(a|abbr|acronym|audio|b|bdi|bdo|big|br|button|canvas|cite|code|data|datalist|del|dfn|em|embed|i|iframe|img|input|ins|kbd|label|map|mark|meter|noscript|object|output|picture|progress|q|ruby|s|samp|slot|small|span|strong|sub|sup|svg|template|textarea|time|u|tt|var|video|wbr)$/, fb = (a, b = {}) => {
  const c = b.addDoctype, d = b.pretty;
  a = eb(a, b, {});
  return c ? `<!doctype html>${d ? C : ""}${a}` : a;
};
function eb(a, b = {}, c = {}, d = !1, e = !1, f) {
  if (null == a || "boolean" == typeof a) {
    return "";
  }
  const {pretty:g = !1, shallow:k = !1, renderRootComponent:l = !1, shallowHighOrder:h = !1, sortAttributes:m, allAttributes:p, xml:n, initialPadding:q = 0, closeVoidTags:t = !1} = b;
  let {lineLength:A = 40} = b;
  A -= q;
  let {nodeName:r, attributes:z = {}} = a;
  var v = ["textarea", "pre"].includes(r);
  const P = " ".repeat(q), H = "string" == typeof g ? g : `  ${P}`;
  if ("object" != typeof a && !r) {
    return Xa(a);
  }
  if ("function" == typeof r) {
    if (!k || !d && l) {
      return v = $a(a), r.prototype && "function" == typeof r.prototype.render ? (a = new r(v, c), a._disable = a.__x = !0, a.props = v, a.context = c, r.getDerivedStateFromProps ? a.state = {...a.state, ...r.getDerivedStateFromProps(a.props, a.state)} : a.componentWillMount && a.componentWillMount(), v = a.render(a.props, a.state, a.context), a.getChildContext && (c = {...c, ...a.getChildContext()})) : v = r(v, c), eb(v, b, c, h, e, f);
    }
    r = r.displayName || r !== Function && r.name || gb(r);
  }
  let u = "";
  ({K:N, I:d, w:f} = ab(z, r, {allAttributes:p, xml:n, J:e, sort:m, w:f}));
  if (g) {
    let O = `<${r}`.length;
    u = N.reduce((I, w) => {
      const B = O + 1 + w.length;
      if (B > A) {
        return O = H.length, `${I}${C}${H}${w}`;
      }
      O = B;
      return `${I} ${w}`;
    }, "");
  } else {
    u = N.length ? " " + N.join(" ") : "";
  }
  u = `<${r}${u}>`;
  if (`${r}`.match(/[\s\\/='"\0<>]/)) {
    throw Error(`Node name ${u} contains unexpected symbols.`);
  }
  var N = `${r}`.match(cb);
  t && N && (u = u.replace(/>$/, " />"));
  let K = [];
  if (d) {
    !v && g && (Ya(d) || d.length + hb(u) > A) && (d = C + H + `${d}`.replace(/(\n+)/g, "$1" + (H || "\t"))), u += d;
  } else {
    if (a.children) {
      let O = g && u.includes("\n");
      const I = [];
      K = a.children.map((w, B) => {
        if (null != w && !1 !== w) {
          var D = eb(w, b, c, !0, "svg" == r ? !0 : "foreignObject" == r ? !1 : e, f);
          if (D) {
            g && D.length + hb(u) > A && (O = !0);
            if ("string" == typeof w.nodeName) {
              const x = D.replace(new RegExp(`</${w.nodeName}>$`), "");
              ib(w.nodeName, x) && (I[B] = D.length);
            }
            return D;
          }
        }
      }).filter(Boolean);
      g && O && !v && (K = K.reduce((w, B, D) => {
        var x = (D = I[D - 1]) && /^<([\s\S]+?)>/.exec(B);
        x && ([, x] = x, x = !db.test(x));
        if (D && !x) {
          x = /[^<]*?(\s)/y;
          var F;
          let Q = !0, W;
          for (; null !== (F = x.exec(B));) {
            const [ea] = F;
            [, W] = F;
            x.lastIndex + ea.length - 1 > A - (Q ? D : 0) && (F = B.slice(0, x.lastIndex - 1), B = B.slice(x.lastIndex), Q ? (w.push(F), Q = !1) : w.push(C + H + `${F}`.replace(/(\n+)/g, "$1" + (H || "\t"))), x.lastIndex = 0);
          }
          W && w.push(W);
          w.push(B);
        } else {
          w.push(C + H + `${B}`.replace(/(\n+)/g, "$1" + (H || "\t")));
        }
        return w;
      }, []));
    }
  }
  if (K.length) {
    u += K.join("");
  } else {
    if (n) {
      return u.substring(0, u.length - 1) + " />";
    }
  }
  N || (!ib(r, K[K.length - 1]) && !v && g && u.includes("\n") && (u += `${C}${P}`), u += `</${r}>`);
  return u;
}
const ib = (a, b) => `${a}`.match(db) && (b ? !/>$/.test(b) : !0);
function gb(a) {
  var b = (Function.prototype.toString.call(a).match(/^\s*function\s+([^( ]+)/) || "")[1];
  if (!b) {
    b = -1;
    for (let c = bb.length; c--;) {
      if (bb[c] === a) {
        b = c;
        break;
      }
    }
    0 > b && (b = bb.push(a) - 1);
    b = `UnnamedComponent${b}`;
  }
  return b;
}
const hb = a => {
  a = a.split(/\r?\n/);
  return a[a.length - 1].length;
};
function kb(a) {
  if ("object" != typeof a) {
    return !1;
  }
  const b = a.re instanceof RegExp;
  a = -1 != ["string", "function"].indexOf(typeof a.replacement);
  return b && a;
}
const lb = (a, b) => {
  if (!(b instanceof Error)) {
    throw b;
  }
  [, , a] = a.stack.split("\n", 3);
  a = b.stack.indexOf(a);
  if (-1 == a) {
    throw b;
  }
  a = b.stack.substr(0, a - 1);
  const c = a.lastIndexOf("\n");
  b.stack = a.substr(0, c);
  throw b;
};
function mb(a, b) {
  function c() {
    return b.filter(kb).reduce((d, {re:e, replacement:f}) => {
      if (this.j) {
        return d;
      }
      if ("string" == typeof f) {
        return d = d.replace(e, f);
      }
      {
        let g;
        return d.replace(e, (k, ...l) => {
          g = Error();
          try {
            return this.j ? k : f.call(this, k, ...l);
          } catch (h) {
            lb(g, h);
          }
        });
      }
    }, `${a}`);
  }
  c.g = () => {
    c.j = !0;
  };
  return c.call(c);
}
;const nb = a => new RegExp(`%%_RESTREAM_${a.toUpperCase()}_REPLACEMENT_(\\d+)_%%`, "g"), ob = (a, b) => `%%_RESTREAM_${a.toUpperCase()}_REPLACEMENT_${b}_%%`;
async function pb(a, b) {
  return qb(a, b);
}
class rb extends va {
  constructor(a, b) {
    super(b);
    this.g = (Array.isArray(a) ? a : [a]).filter(kb);
    this.j = !1;
    this.s = b;
  }
  async replace(a, b) {
    const c = new rb(this.g, this.s);
    b && Object.assign(c, b);
    a = await pb(c, a);
    c.j && (this.j = !0);
    b && Object.keys(b).forEach(d => {
      b[d] = c[d];
    });
    return a;
  }
  async reduce(a) {
    return await this.g.reduce(async(b, {re:c, replacement:d}) => {
      b = await b;
      if (this.j) {
        return b;
      }
      if ("string" == typeof d) {
        b = b.replace(c, d);
      } else {
        const e = [];
        let f;
        const g = b.replace(c, (k, ...l) => {
          f = Error();
          try {
            if (this.j) {
              return e.length ? e.push(Promise.resolve(k)) : k;
            }
            const h = d.call(this, k, ...l);
            h instanceof Promise && e.push(h);
            return h;
          } catch (h) {
            lb(f, h);
          }
        });
        if (e.length) {
          try {
            const k = await Promise.all(e);
            b = b.replace(c, () => k.shift());
          } catch (k) {
            lb(f, k);
          }
        } else {
          b = g;
        }
      }
      return b;
    }, `${a}`);
  }
  async _transform(a, b, c) {
    try {
      const d = await this.reduce(a);
      this.push(d);
      c();
    } catch (d) {
      a = ra(d.stack), d.stack = a, c(d);
    }
  }
}
async function qb(a, b) {
  b instanceof ta ? b.pipe(a) : a.end(b);
  return await za(a);
}
;var sb = tty;
const tb = util.debuglog, ub = util.format, vb = util.inspect;
/*

 Copyright (c) 2016 Zeit, Inc.
 https://npmjs.org/ms
*/
function wb(a) {
  var b = {}, c = typeof a;
  if ("string" == c && 0 < a.length) {
    return xb(a);
  }
  if ("number" == c && isFinite(a)) {
    return b.S ? (b = Math.abs(a), a = 864E5 <= b ? yb(a, b, 864E5, "day") : 36E5 <= b ? yb(a, b, 36E5, "hour") : 6E4 <= b ? yb(a, b, 6E4, "minute") : 1000 <= b ? yb(a, b, 1000, "second") : a + " ms") : (b = Math.abs(a), a = 864E5 <= b ? Math.round(a / 864E5) + "d" : 36E5 <= b ? Math.round(a / 36E5) + "h" : 6E4 <= b ? Math.round(a / 6E4) + "m" : 1000 <= b ? Math.round(a / 1000) + "s" : a + "ms"), a;
  }
  throw Error("val is not a non-empty string or a valid number. val=" + JSON.stringify(a));
}
function xb(a) {
  a = String(a);
  if (!(100 < a.length) && (a = /^(-?(?:\d+)?\.?\d+) *(milliseconds?|msecs?|ms|seconds?|secs?|s|minutes?|mins?|m|hours?|hrs?|h|days?|d|weeks?|w|years?|yrs?|y)?$/i.exec(a))) {
    var b = parseFloat(a[1]);
    switch((a[2] || "ms").toLowerCase()) {
      case "years":
      case "year":
      case "yrs":
      case "yr":
      case "y":
        return 315576E5 * b;
      case "weeks":
      case "week":
      case "w":
        return 6048E5 * b;
      case "days":
      case "day":
      case "d":
        return 864E5 * b;
      case "hours":
      case "hour":
      case "hrs":
      case "hr":
      case "h":
        return 36E5 * b;
      case "minutes":
      case "minute":
      case "mins":
      case "min":
      case "m":
        return 6E4 * b;
      case "seconds":
      case "second":
      case "secs":
      case "sec":
      case "s":
        return 1000 * b;
      case "milliseconds":
      case "millisecond":
      case "msecs":
      case "msec":
      case "ms":
        return b;
    }
  }
}
function yb(a, b, c, d) {
  return Math.round(a / c) + " " + d + (b >= 1.5 * c ? "s" : "");
}
;/*
 bytes
 Copyright(c) 2012-2014 TJ Holowaychuk
 Copyright(c) 2015 Jed Watson
 MIT Licensed
*/
const zb = /\B(?=(\d{3})+(?!\d))/g, Ab = /(?:\.0*|(\.[^0]+)0+)$/, R = {b:1, kb:1024, mb:1048576, gb:1073741824, tb:Math.pow(1024, 4), pb:Math.pow(1024, 5)};
function S(a, b) {
  if (!Number.isFinite(a)) {
    return null;
  }
  const c = Math.abs(a), d = b && b.T || "", e = b && b.V || "", f = b && void 0 !== b.G ? b.G : 2, g = !(!b || !b.P);
  (b = b && b.U || "") && R[b.toLowerCase()] || (b = c >= R.pb ? "PB" : c >= R.tb ? "TB" : c >= R.gb ? "GB" : c >= R.mb ? "MB" : c >= R.kb ? "KB" : "B");
  a = (a / R[b.toLowerCase()]).toFixed(f);
  g || (a = a.replace(Ab, "$1"));
  d && (a = a.replace(zb, d));
  return a + e + b;
}
;/*
 diff package https://github.com/kpdecker/jsdiff
 BSD License
 Copyright (c) 2009-2015, Kevin Decker <kpdecker@gmail.com>
*/
function Bb(a, b, c) {
  let d = a[a.length - 1];
  d && d.l === b && d.u === c ? a[a.length - 1] = {count:d.count + 1, l:b, u:c} : a.push({count:1, l:b, u:c});
}
function Cb(a, b, c, d, e) {
  let f = c.length, g = d.length, k = b.i;
  e = k - e;
  let l = 0;
  for (; k + 1 < f && e + 1 < g && a.equals(c[k + 1], d[e + 1]);) {
    k++, e++, l++;
  }
  l && b.m.push({count:l});
  b.i = k;
  return e;
}
function Db(a) {
  let b = [];
  for (let c = 0; c < a.length; c++) {
    a[c] && b.push(a[c]);
  }
  return b;
}
class Eb {
  diff(a, b) {
    a = Db(a.split(""));
    b = Db(b.split(""));
    let c = b.length, d = a.length, e = 1, f = c + d, g = [{i:-1, m:[]}];
    var k = Cb(this, g[0], b, a, 0);
    if (g[0].i + 1 >= c && k + 1 >= d) {
      return [{value:this.join(b), count:b.length}];
    }
    for (; e <= f;) {
      a: {
        for (k = -1 * e; k <= e; k += 2) {
          var l = void 0;
          l = g[k - 1];
          let m = g[k + 1];
          var h = (m ? m.i : 0) - k;
          l && (g[k - 1] = void 0);
          let p = l && l.i + 1 < c;
          h = m && 0 <= h && h < d;
          if (p || h) {
            !p || h && l.i < m.i ? (l = {i:m.i, m:m.m.slice(0)}, Bb(l.m, void 0, !0)) : (l.i++, Bb(l.m, !0, void 0));
            h = Cb(this, l, b, a, k);
            if (l.i + 1 >= c && h + 1 >= d) {
              k = Fb(this, l.m, b, a);
              break a;
            }
            g[k] = l;
          } else {
            g[k] = void 0;
          }
        }
        e++;
        k = void 0;
      }
      if (k) {
        return k;
      }
    }
  }
  equals(a, b) {
    return a === b;
  }
  join(a) {
    return a.join("");
  }
}
function Fb(a, b, c, d) {
  let e = 0, f = b.length, g = 0, k = 0;
  for (; e < f; e++) {
    var l = b[e];
    if (l.u) {
      l.value = a.join(d.slice(k, k + l.count)), k += l.count, e && b[e - 1].l && (l = b[e - 1], b[e - 1] = b[e], b[e] = l);
    } else {
      if (l.l) {
        l.value = a.join(c.slice(g, g + l.count));
      } else {
        let h = c.slice(g, g + l.count);
        h = h.map(function(m, p) {
          p = d[k + p];
          return p.length > m.length ? p : m;
        });
        l.value = a.join(h);
      }
      g += l.count;
      l.l || (k += l.count);
    }
  }
  c = b[f - 1];
  1 < f && "string" === typeof c.value && (c.l || c.u) && a.equals("", c.value) && (b[f - 2].value += c.value, b.pop());
  return b;
}
;const Gb = {black:30, red:31, green:32, yellow:33, blue:34, magenta:35, cyan:36, white:37, grey:90}, Hb = {black:40, red:41, green:42, yellow:43, blue:44, magenta:45, cyan:46, white:47};
function V(a, b) {
  return (b = Gb[b]) ? `\x1b[${b}m${a}\x1b[0m` : a;
}
function Ib(a, b) {
  return (b = Hb[b]) ? `\x1b[${b}m${a}\x1b[0m` : a;
}
;var Jb = {f:S, ["fy"](a) {
  return V(S(a) || "", "yellow");
}, ["fr"](a) {
  return V(S(a) || "", "red");
}, ["fb"](a) {
  return V(S(a) || "", "blue");
}, ["fg"](a) {
  return V(S(a) || "", "green");
}, ["fc"](a) {
  return V(S(a) || "", "cyan");
}, ["fm"](a) {
  return V(S(a) || "", "magenta");
}};
const Kb = Object.keys(process.env).filter(a => /^debug_/i.test(a)).reduce((a, b) => {
  const c = b.substring(6).toLowerCase().replace(/_([a-z])/g, (d, e) => e.toUpperCase());
  b = process.env[b];
  /^(yes|on|true|enabled)$/i.test(b) ? b = !0 : /^(no|off|false|disabled)$/i.test(b) ? b = !1 : "null" === b ? b = null : b = Number(b);
  a[c] = b;
  return a;
}, {}), Lb = {init:function(a) {
  a.inspectOpts = {...Kb};
}, log:function(...a) {
  return process.stderr.write(ub(...a) + "\n");
}, formatArgs:function(a) {
  var b = this.namespace, c = this.color;
  const d = this.diff;
  this.useColors ? (c = "\u001b[3" + (8 > c ? c : "8;5;" + c), b = `  ${c};1m${b} \u001B[0m`, a[0] = b + a[0].split("\n").join("\n" + b), a.push(c + "m+" + wb(d) + "\u001b[0m")) : a[0] = (Kb.hideDate ? "" : (new Date).toISOString() + " ") + b + " " + a[0];
}, save:function(a) {
  a ? process.env.DEBUG = a : delete process.env.DEBUG;
}, load:function() {
  return process.env.DEBUG;
}, useColors:function() {
  return "colors" in Kb ? !!Kb.colors : sb.isatty(process.stderr.fd);
}, colors:[6, 2, 3, 4, 5, 1], inspectOpts:Kb, formatters:{o:function(a) {
  return vb(a, {...this.inspectOpts, colors:this.useColors}).replace(/\s*\n\s*/g, " ");
}, O:function(a) {
  return vb(a, {...this.inspectOpts, colors:this.useColors});
}, ...Jb}};
function Mb(a) {
  function b(...g) {
    if (b.enabled) {
      var k = Number(new Date);
      b.diff = k - (f || k);
      b.prev = f;
      f = b.curr = k;
      g[0] = Nb(g[0]);
      "string" != typeof g[0] && g.unshift("%O");
      var l = 0;
      g[0] = g[0].replace(/%([a-zA-Z%]+)/g, (h, m) => {
        if ("%%" == h) {
          return h;
        }
        l++;
        if (m = c[m]) {
          h = m.call(b, g[l]), g.splice(l, 1), l--;
        }
        return h;
      });
      d.call(b, g);
      (b.log || e).apply(b, g);
    }
  }
  const c = a.formatters, d = a.formatArgs, e = a.log;
  let f;
  return b;
}
function Ob(a) {
  const b = Mb(a);
  "function" == typeof a.init && a.init(b);
  a.g.push(b);
  return b;
}
function Pb(a, b) {
  let c = 0;
  for (let d = 0; d < b.length; d++) {
    c = (c << 5) - c + b.charCodeAt(d), c |= 0;
  }
  return a.colors[Math.abs(c) % a.colors.length];
}
function Qb(a) {
  var b = Lb.load();
  a.save(b);
  a.s = [];
  a.v = [];
  let c;
  const d = ("string" == typeof b ? b : "").split(/[\s,]+/), e = d.length;
  for (c = 0; c < e; c++) {
    d[c] && (b = d[c].replace(/\*/g, ".*?"), "-" == b[0] ? a.v.push(new RegExp("^" + b.substr(1) + "$")) : a.s.push(new RegExp("^" + b + "$")));
  }
  for (c = 0; c < a.g.length; c++) {
    b = a.g[c], b.enabled = a.enabled(b.namespace);
  }
}
class Rb {
  constructor(a) {
    this.colors = a.colors;
    this.formatArgs = a.formatArgs;
    this.inspectOpts = a.inspectOpts;
    this.log = a.log;
    this.save = a.save;
    this.init = a.init;
    this.formatters = a.formatters || {};
    this.g = [];
    this.s = [];
    this.v = [];
  }
  destroy(a) {
    a = this.g.indexOf(a);
    return -1 !== a ? (this.g.splice(a, 1), !0) : !1;
  }
  enabled(a) {
    if ("*" == a[a.length - 1]) {
      return !0;
    }
    let b, c;
    b = 0;
    for (c = this.v.length; b < c; b++) {
      if (this.v[b].test(a)) {
        return !1;
      }
    }
    b = 0;
    for (c = this.s.length; b < c; b++) {
      if (this.s[b].test(a)) {
        return !0;
      }
    }
    return !1;
  }
}
function Nb(a) {
  return a instanceof Error ? a.stack || a.message : a;
}
;const Sb = a => {
  a = `(${a.join("|")})`;
  return new RegExp(`(\\r?\\n)?( *)(<${a}${"(?:\\s+(?!\\/>)[^>]*?)?"}(?:\\s*?/>|>[\\s\\S]*?<\\/\\4>))`, "gm");
};
const Tb = _crypto.createHash;
require("./init");
require("./make-io");
require("./start");
require("./preact-proxy");
require("./start-plain");
var Ub;
Ub = function() {
  const a = new Rb(Lb);
  return function(b) {
    const c = Ob(a);
    c.namespace = b;
    c.useColors = Lb.useColors();
    c.enabled = a.enabled(b);
    c.color = Pb(a, b);
    c.destroy = function() {
      a.destroy(this);
    };
    c.extend = function(d, e) {
      d = this.namespace + (void 0 === e ? ":" : e) + d;
      d.log = this.log;
      return d;
    };
    Qb(a);
    return c;
  };
}()("competent");
const Vb = (a, b, c) => {
  let d;
  "string" == typeof a ? d = a : Array.isArray(a) ? d = a.map(e => "string" == typeof e ? e : fb(e, b)).join(c) : d = fb(a, b);
  return d;
}, Xb = async({getReplacements:a, key:b, D:c, re:d, replacement:e, getContext:f, A:g, position:k, body:l}) => {
  let h;
  a ? h = a(b, c) : c ? h = {re:Wb(d, b), replacement:e} : h = {re:d, replacement:e};
  a = new rb(h);
  f && (b = f(g, {position:k, key:b}), Object.assign(a, b));
  return await pb(a, l);
}, Wb = (a, b) => new RegExp(a.source.replace(new RegExp(`([|(])${b}([|)])`), (c, d, e) => "|" == d && "|" == e ? "|" : ")" == e ? e : "(" == d ? d : ""), a.flags);
const Yb = async a => {
  try {
    return await J(ca, a);
  } catch (b) {
    return null;
  }
};
const X = async(a, b) => {
  b && (b = L(b), a = M(b, a));
  var c = await Yb(a);
  b = a;
  let d = !1;
  if (!c) {
    if (b = await Zb(a), !b) {
      throw Error(`${a}.js or ${a}.jsx is not found.`);
    }
  } else {
    if (c.isDirectory()) {
      c = !1;
      let e;
      a.endsWith("/") || (e = b = await Zb(a), c = !0);
      if (!e) {
        b = await Zb(M(a, "index"));
        if (!b) {
          throw Error(`${c ? `${a}.jsx? does not exist, and ` : ""}index.jsx? file is not found in ${a}`);
        }
        d = !0;
      }
    }
  }
  return {path:a.startsWith(".") ? Da("", b) : b, R:d};
}, Zb = async a => {
  a = `${a}.js`;
  let b = await Yb(a);
  b || (a = `${a}x`);
  if (b = await Yb(a)) {
    return a;
  }
};
const $b = child_process.fork, ac = child_process.spawn;
const bc = async a => {
  const [b, c, d] = await Promise.all([new Promise((e, f) => {
    a.on("error", f).on("exit", g => {
      e(g);
    });
  }), a.stdout ? za(a.stdout) : void 0, a.stderr ? za(a.stderr) : void 0]);
  return {code:b, stdout:c, stderr:d};
};
const cc = (a, b) => a.some(c => c == b), dc = (a, b) => {
  const c = cc(a, "index.md"), d = cc(a, "footer.md"), e = ["index.md", "footer.md"];
  a = a.filter(f => !e.includes(f)).sort((f, g) => {
    f = f.localeCompare(g, void 0, {numeric:!0});
    return b ? -f : f;
  });
  return c && d ? ["index.md", ...a, "footer.md"] : c ? ["index.md", ...a] : d ? [...a, "footer.md"] : a;
};
const ec = tb("pedantry"), gc = async({stream:a, source:b, path:c = ".", content:d = {}, reverse:e = !1, separator:f, includeFilename:g, ignoreHidden:k}) => {
  var l = Object.keys(d);
  l = await dc(l, e).reduce(async(h, m) => {
    h = await h;
    const {type:p, content:n} = d[m], q = M(c, m);
    let t;
    "File" == p ? k && m.startsWith(".") || (t = await fc({stream:a, source:b, path:q, separator:f, includeFilename:g})) : "Directory" == p && (t = await gc({stream:a, source:b, path:q, content:n, reverse:e, separator:f, includeFilename:g, ignoreHidden:k}));
    return h + t;
  }, 0);
  ec("dir %s size: %s B", c, l);
  return l;
}, fc = async a => {
  const b = a.stream, c = a.path, d = a.separator, e = a.includeFilename, f = M(a.source, c);
  b.emit("file", c);
  d && !b.B && (e ? b.push({file:"separator", data:d}) : b.push(d));
  a = await new Promise((g, k) => {
    let l = 0;
    const h = aa(f);
    h.on("data", m => {
      l += m.byteLength;
    }).on("error", m => {
      k(m);
    }).on("close", () => {
      g(l);
    });
    if (e) {
      h.on("data", m => {
        b.push({file:f, data:`${m}`});
      });
    } else {
      h.pipe(b, {end:!1});
    }
  });
  b.B = !1;
  ec("file %s :: %s B", f, a);
  return a;
};
class hc extends ua {
  constructor(a, b = {}) {
    const {reverse:c = !1, addNewLine:d = !1, addBlankLine:e = !1, includeFilename:f = !1, ignoreHidden:g = !1, sep:k = C} = b;
    super({objectMode:f});
    let l;
    d ? l = k : e && (l = `${k}${k}`);
    this.B = !0;
    (async() => {
      let h;
      try {
        ({content:h} = await La(a));
      } catch (m) {
        const p = Error(m.message);
        p.code = m.code;
        this.emit("error", p);
      }
      try {
        await gc({stream:this, source:a, content:h, reverse:c, separator:l, includeFilename:f, ignoreHidden:g});
      } catch (m) {
        this.emit("error", m);
      } finally {
        this.end();
      }
    })();
  }
}
;const ic = _module.builtinModules;
const jc = /^ *import(?:\s+(?:[^\s,]+)\s*,?)?(?:\s*{(?:[^}]+)})?\s+from\s+(['"])(.+?)\1/gm, kc = /^ *import\s+(?:.+?\s*,\s*)?\*\s+as\s+.+?\s+from\s+(['"])(.+?)\1/gm, lc = /^ *import\s+(['"])(.+?)\1/gm, mc = /^ *export\s+(?:{[^}]+?}|\*)\s+from\s+(['"])(.+?)\1/gm, nc = a => [jc, kc, lc, mc].reduce((b, c) => {
  c = Pa(c, a, ["q", "from"]).map(d => d.from);
  return [...b, ...c];
}, []);
let oc;
const qc = async(a, b, c = {}) => {
  oc || ({root:oc} = Ca(process.cwd()));
  const {fields:d, soft:e = !1} = c;
  var f = M(a, "node_modules", b);
  f = M(f, "package.json");
  const g = await Yb(f);
  if (g) {
    a = await pc(f, d);
    if (void 0 === a) {
      throw Error(`The package ${Da("", f)} does export the module.`);
    }
    if (!a.entryExists && !e) {
      throw Error(`The exported module ${a.main} in package ${b} does not exist.`);
    }
    const {entry:k, version:l, packageName:h, main:m, entryExists:p, ...n} = a;
    return {entry:Da("", k), packageJson:Da("", f), ...l ? {version:l} : {}, packageName:h, ...m ? {hasMain:!0} : {}, ...p ? {} : {entryExists:!1}, ...n};
  }
  if (a == oc && !g) {
    throw Error(`Package.json for module ${b} not found.`);
  }
  return qc(M(Ea(a), ".."), b, c);
}, pc = async(a, b = []) => {
  const c = await Aa(a);
  let d, e, f, g, k;
  try {
    ({module:d, version:e, name:f, main:g, ...k} = JSON.parse(c)), k = b.reduce((h, m) => {
      h[m] = k[m];
      return h;
    }, {});
  } catch (h) {
    throw Error(`Could not parse ${a}.`);
  }
  a = L(a);
  b = d || g;
  if (!b) {
    if (!await Yb(M(a, "index.js"))) {
      return;
    }
    b = g = "index.js";
  }
  a = M(a, b);
  let l;
  try {
    ({path:l} = await X(a)), a = l;
  } catch (h) {
  }
  return {entry:a, version:e, packageName:f, main:!d && g, entryExists:!!l, ...k};
};
const rc = a => /^[./]/.test(a), sc = async(a, b, c, d, e = null) => {
  const f = G(), g = L(a);
  b = b.map(async k => {
    if (ic.includes(k)) {
      return {internal:k};
    }
    if (/^[./]/.test(k)) {
      try {
        var {path:l} = await X(k, a);
        return {entry:l, package:e};
      } catch (h) {
      }
    } else {
      {
        let [p, n, ...q] = k.split("/");
        !p.startsWith("@") && n ? (q = [n, ...q], n = p) : n = p.startsWith("@") ? `${p}/${n}` : p;
        l = {name:n, paths:q.join("/")};
      }
      const {name:h, paths:m} = l;
      if (m) {
        const {packageJson:p, packageName:n} = await qc(g, h);
        k = L(p);
        ({path:k} = await X(M(k, m)));
        return {entry:k, package:n};
      }
    }
    try {
      const {entry:h, packageJson:m, version:p, packageName:n, hasMain:q, ...t} = await qc(g, k, {fields:d});
      return n == e ? (console.warn("[static-analysis] Skipping package %s that imports itself in %s", n, a), null) : {entry:h, packageJson:m, version:p, name:n, ...q ? {hasMain:q} : {}, ...t};
    } catch (h) {
      if (c) {
        return null;
      }
      [k] = process.version.split(".");
      k = parseInt(k.replace("v", ""), 10);
      if (12 <= k) {
        throw h;
      }
      throw f(h);
    }
  });
  return (await Promise.all(b)).filter(Boolean);
}, uc = async(a, b = {}, {nodeModules:c = !0, shallow:d = !1, soft:e = !1, fields:f = [], L:g = {}, mergeSameNodeModules:k = !0, package:l} = {}) => {
  if (a in b) {
    return [];
  }
  b[a] = 1;
  var h = await Aa(a), m = nc(h);
  h = tc(h);
  m = c ? m : m.filter(rc);
  h = c ? h : h.filter(rc);
  try {
    const n = await sc(a, m, e, f, l), q = await sc(a, h, e, f, l);
    q.forEach(t => {
      t.required = !0;
    });
    var p = [...n, ...q];
  } catch (n) {
    throw n.message = `${a}\n [!] ${n.message}`, n;
  }
  l = k ? p.map(n => {
    var q = n.name, t = n.version;
    const A = n.required;
    if (q && t) {
      q = `${q}:${t}${A ? "-required" : ""}`;
      if (t = g[q]) {
        return t;
      }
      g[q] = n;
    }
    return n;
  }) : p;
  p = l.map(n => ({...n, from:a}));
  return await l.filter(({entry:n}) => n && !(n in b)).reduce(async(n, {entry:q, hasMain:t, packageJson:A, name:r, package:z}) => {
    if (A && d) {
      return n;
    }
    n = await n;
    r = (await uc(q, b, {nodeModules:c, shallow:d, soft:e, fields:f, package:r || z, L:g, mergeSameNodeModules:k})).map(v => ({...v, from:v.from ? v.from : q, ...!v.packageJson && t ? {hasMain:t} : {}}));
    return [...n, ...r];
  }, p);
}, tc = a => Pa(/(?:^|[^\w\d_])require\(\s*(['"])(.+?)\1\s*\)/gm, a, ["q", "from"]).map(b => b.from);
const vc = async a => {
  const b = G();
  a = Array.isArray(a) ? a : [a];
  a = await Promise.all(a.map(async l => {
    ({path:l} = await X(l));
    return l;
  }));
  const {nodeModules:c = !0, shallow:d = !1, soft:e = !1, fields:f = [], mergeSameNodeModules:g = !0} = {shallow:!0, soft:!0};
  let k;
  try {
    const l = {};
    k = await a.reduce(async(h, m) => {
      h = await h;
      m = await uc(m, l, {nodeModules:c, shallow:d, soft:e, fields:f, mergeSameNodeModules:g});
      h.push(...m);
      return h;
    }, []);
  } catch (l) {
    [a] = process.version.split(".");
    a = parseInt(a.replace("v", ""), 10);
    if (12 <= a) {
      throw l;
    }
    throw b(l);
  }
  return k.filter(({internal:l, entry:h}, m) => l ? k.findIndex(({internal:p}) => p == l) == m : k.findIndex(({entry:p}) => h == p) == m).map(l => {
    const h = l.entry, m = l.internal, p = k.filter(({internal:n, entry:q}) => {
      if (m) {
        return m == n;
      }
      if (h) {
        return h == q;
      }
    }).map(({from:n}) => n).filter((n, q, t) => t.indexOf(n) == q);
    return {...l, from:p};
  }).map(({package:l, ...h}) => l ? {package:l, ...h} : h);
};
const xc = (a, b, c = console.log) => {
  const d = [], e = [];
  b.forEach(f => {
    a.includes(f) || d.push(f);
  });
  a.forEach(f => {
    b.includes(f) || e.push(f);
  });
  if (!d.length && !e.length) {
    return !0;
  }
  d.forEach(f => {
    const {entry:g, C:k} = wc(f);
    c(V("+", "green"), g, k);
  });
  e.forEach(f => {
    const {entry:g, C:k} = wc(f);
    c(V("-", "red"), g, k);
  });
  return !1;
}, wc = a => {
  const [b, c] = a.split(" ");
  a = "";
  c && (a = /^\d+$/.test(c) ? (new Date(parseInt(c, 10))).toLocaleString() : c);
  return {entry:b, C:a};
}, yc = async a => (await J(ca, a)).mtime.getTime(), zc = async a => await Promise.all(a.map(async({entry:b, name:c, internal:d, version:e}) => {
  if (c) {
    return `${c} ${e}`;
  }
  if (d) {
    return d;
  }
  c = await yc(b);
  return `${b} ${c}`;
})), Ac = async a => {
  const b = await vc(a), c = await zc(b);
  ({path:a} = await X(a));
  return {mtime:await yc(a), hash:c, N:b};
};
const Bc = async(a, b, c) => {
  if (b.path == a || c == a) {
    ({promise:c} = new ya({rs:b}));
    const d = await c;
    await new Promise((e, f) => {
      ba(a).once("error", f).end(d, e);
    });
  } else {
    await new Promise((d, e) => {
      const f = ba(a);
      b.pipe(f);
      f.once("error", e).on("close", d);
    });
  }
};
module.exports = {competent:(a, b = {}) => {
  async function c(q, t, A, r, z, v, P) {
    Ub("render %s", z);
    const H = Error("Skip render");
    try {
      const u = a[z], N = P.slice(0, v), K = P.slice(v + q.length);
      if (/\x3c!--\s*$/.test(N) && /^\s*--\x3e/.test(K)) {
        return q;
      }
      const [{content:O = "", props:I}] = Ta(z, r);
      r = [O];
      let w = !1, B = !0, D = !1;
      const x = {pretty:void 0, lineLength:void 0};
      let F, Q, W, ea;
      const ia = e.call(this, {...I, children:r}, {export(y = !0, E = null) {
        w = y;
        E && (ea = Object.entries(E).reduce((T, [U, jb]) => {
          if (void 0 === jb) {
            return T;
          }
          T[U] = jb;
          return T;
        }, {}));
      }, setPretty(y, E) {
        x.pretty = y;
        E && (x.lineLength = E);
      }, renderAgain(y = !0, E = !1) {
        B = y;
        D = E;
      }, setChildContext(y) {
        Q = y;
      }, removeLine(y = !0) {
        W = y;
      }, skipRender() {
        throw H;
      }}, z, v, P);
      let Y;
      try {
        const y = u.call(this, ia);
        Y = y instanceof Promise ? await y : y;
      } catch (y) {
        if (!y.message.startsWith("Class constructor")) {
          throw y;
        }
        const E = new u, T = E.serverRender ? E.serverRender(ia) : E.render(ia);
        Y = T instanceof Promise ? await T : T;
        if (E.fileRender) {
          let U = await E.render(ia);
          U = Vb(U, x, p);
          B && (U = await Xb({getContext:h.bind(this), getReplacements:m.bind(this), key:z, D, re:n, replacement:c, A:Q, body:U}));
          await E.fileRender(U, ia);
        }
      }
      if (w) {
        const y = Array.isArray(Y) ? Y[0] : Y;
        F = y.attributes.id;
        F || (F = d.call(this, z, ea || I), y.attributes.id = F);
      }
      let Z = Vb(Y, x, p);
      if (!Z && W) {
        return g && g.call(this, z, I), "";
      }
      Z = (t || "") + Z.replace(/(^|(?:\r?\n))/g, `$1${A}`);
      B && (Z = await Xb({getContext:h ? h.bind(this) : void 0, getReplacements:m ? m.bind(this) : void 0, key:z, D, re:n, replacement:c, A:Q, body:Z, position:v}));
      w && f.call(this, z, F, ea || I, r);
      g && g.call(this, z, I);
      return Z;
    } catch (u) {
      if (u === H) {
        return q;
      }
      k && k.call(this, z, u, v, P);
      return l ? "" : q;
    }
  }
  const {getId:d, getProps:e = (q, t) => ({...q, ...t}), markExported:f, onSuccess:g, onFail:k, removeOnError:l = !1, getContext:h, getReplacements:m, sep:p = C} = b, n = Sb(Object.keys(a));
  return {re:n, replacement:c};
}, c:V, b:Ib, readDirStructure:La, clone:async(a, b) => {
  const c = await J(ca, a), d = Ba(a);
  b = M(b, d);
  c.isDirectory() ? await Oa(a, b) : c.isSymbolicLink() ? await Na(a, b) : (await Fa(b), await Ma(a, b));
}, Pedantry:hc, whichStream:async function(a) {
  const b = a.source, c = a.destination;
  let {readable:d, writable:e} = a;
  if (!b && !d) {
    throw Error("Please give either a source or readable.");
  }
  if (!c && !e) {
    throw Error("Please give either a destination or writable.");
  }
  b && !d && (d = aa(b));
  "-" == c ? d.pipe(process.stdout) : c ? await Bc(c, d, b) : e instanceof wa && (d.pipe(e), await new Promise((f, g) => {
    e.on("error", g);
    e.on("finish", f);
  }));
}, compare:async(a, b = {}, c = console.log) => {
  b = b[a];
  const {mtime:d, hash:e} = await Ac(a);
  a = Tb("md5").update(JSON.stringify(e)).digest("hex");
  if (!b) {
    return {result:!1, reason:"NO_CACHE", mtime:d, hash:e, md5:a};
  }
  const {mtime:f, hash:g} = b;
  return d != f ? {result:!1, reason:"MTIME_CHANGE", mtime:d, hash:e, currentMtime:f, md5:a} : xc(g, e, c) ? {result:!0, md5:a} : {result:!1, mtime:d, hash:e, reason:"HASH_CHANGE", md5:a};
}, ensurePath:Fa, ensurePathSync:function(a) {
  const b = L(a);
  try {
    return Ha(b), a;
  } catch (c) {
    if (/EEXIST/.test(c.message) && -1 != c.message.indexOf(b)) {
      return a;
    }
    throw c;
  }
}, read:Aa, replace:qb, usually:function(a = {usage:{}}) {
  const {usage:b = {}, description:c, line:d, example:e} = a;
  a = Object.keys(b);
  const f = Object.values(b), [g] = a.reduce(([h = 0, m = 0], p) => {
    const n = b[p].split("\n").reduce((q, t) => t.length > q ? t.length : q, 0);
    n > m && (m = n);
    p.length > h && (h = p.length);
    return [h, m];
  }, []), k = (h, m) => {
    m = " ".repeat(m - h.length);
    return `${h}${m}`;
  };
  a = a.reduce((h, m, p) => {
    p = f[p].split("\n");
    m = k(m, g);
    const [n, ...q] = p;
    m = `${m}\t${n}`;
    const t = k("", g);
    p = q.map(A => `${t}\t${A}`);
    return [...h, m, ...p];
  }, []).map(h => `\t${h}`);
  const l = [c, `  ${d || ""}`].filter(h => h ? h.trim() : h).join("\n\n");
  a = `${l ? `${l}\n` : ""}
${a.join("\n")}
`;
  return e ? `${a}
  Example:

    ${e}
` : a;
}, spawn:function(a, b, c) {
  if (!a) {
    throw Error("Please specify a command to spawn.");
  }
  a = ac(a, b, c);
  b = bc(a);
  a.promise = b;
  a.spawnCommand = a.spawnargs.join(" ");
  return a;
}, fork:function(a, b, c) {
  if (!a) {
    throw Error("Please specify a module to fork");
  }
  a = $b(a, b, c);
  b = bc(a);
  a.promise = b;
  a.spawnCommand = a.spawnargs.join(" ");
  return a;
}, SyncReplaceable:mb, Replaceable:rb, makeMarkers:(a, b) => Object.keys(a).reduce((c, d) => {
  {
    var e = a[d];
    const {getReplacement:f = ob, getRegex:g = nb} = b || {}, k = g(d);
    e = {name:d, re:e, regExp:k, getReplacement:f, map:{}, lastIndex:0};
  }
  return {...c, [d]:e};
}, {}), makeCutRule:a => {
  const b = a.map, c = a.getReplacement, d = a.name;
  return {re:a.re, replacement(e) {
    const f = a.lastIndex;
    b[f] = e;
    a.lastIndex += 1;
    return c(d, f);
  }};
}, makePasteRule:(a, b = []) => {
  const c = a.map;
  return {re:a.regExp, replacement(d, e) {
    d = c[e];
    delete c[e];
    e = Array.isArray(b) ? b : [b];
    return mb(d, e);
  }};
}, resolveDependency:X, rexml:Ta, reduceUsage:a => Object.keys(a).reduce((b, c) => {
  const d = a[c];
  if ("string" == typeof d) {
    return b[`-${d}`] = "", b;
  }
  c = d.command ? c : `--${c}`;
  d.short && (c = `${c}, -${d.short}`);
  let e = d.description;
  d.default && (e = `${e}\nDefault: ${d.default}.`);
  b[c] = e;
  return b;
}, {}), write:async function(a, b) {
  if (!a) {
    throw Error("No path is given.");
  }
  const c = G(!0), d = ba(a);
  await new Promise((e, f) => {
    d.on("error", g => {
      g = c(g);
      f(g);
    }).on("close", e).end(b);
  });
}, argufy:function(a = {}, b = process.argv) {
  let [, , ...c] = b;
  const d = Va(c);
  c = c.slice(d.length);
  a = Object.entries(a).reduce((g, [k, l]) => {
    g[k] = "string" == typeof l ? {short:l} : l;
    return g;
  }, {});
  const e = [];
  a = Object.entries(a).reduce((g, [k, l]) => {
    let h;
    try {
      const m = l.short, p = l.boolean, n = l.number, q = l.command, t = l.multiple;
      if (q && t && d.length) {
        h = d;
      } else {
        if (q && d.length) {
          h = d[0];
        } else {
          const A = Ua(c, k, m, p, n);
          ({value:h} = A);
          const r = A.index, z = A.length;
          void 0 !== r && z && e.push({index:r, length:z});
        }
      }
    } catch (m) {
      return g;
    }
    return void 0 === h ? g : {...g, [k]:h};
  }, {});
  let f = c;
  e.forEach(({index:g, length:k}) => {
    Array.from({length:k}).forEach((l, h) => {
      f[g + h] = null;
    });
  });
  f = f.filter(g => null !== g);
  Object.assign(a, {M:f});
  return a;
}, Catchment:ya, collect:za, clearr:a => {
  let b = !0;
  return a.split(/(\r?\n)/).reduce((c, d, e, f) => {
    if (b = !b) {
      return c;
    }
    d = d.split("\r");
    d = d.reduce((g, k, l) => {
      if (!l) {
        return g;
      }
      g = g.slice(k.length);
      return `${k}${g}`;
    }, d[0]);
    return `${c}${d}${f[e + 1] || ""}`;
  }, "");
}, erte:function(a, b) {
  return (new Eb).diff(a, b).map(({l:c, u:d, value:e}) => {
    let f;
    const g = e.split(" ");
    c ? f = g.map(k => k.replace(/(\r?\n)$/mg, "\u23ce$1")).map(k => V(k, "green")).join(Ib(" ", "green")) : d ? f = g.map(k => k.replace(/(\r?\n)$/mg, "\u23ce$1")).map(k => V(k, "red")).join(Ib(" ", "red")) : f = V(e, "grey");
    return f;
  }).join("");
}, forkfeed:(a, b, c = [], d = null) => {
  if (d) {
    a.on("data", k => d.write(k));
  }
  let [e, ...f] = c;
  if (e) {
    var g = k => {
      const [l, h] = e;
      l.test(k) && (k = `${h}\n`, d && d.write(k), b.write(k), [e, ...f] = f, e || a.removeListener("data", g));
    };
    a.on("data", g);
  }
}, makepromise:J, mismatch:Pa};


//# sourceMappingURL=index.js.map