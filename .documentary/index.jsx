import read from '@wrote/read'
import { repository } from '../package.json'
import { format } from 'url'

/**
 * Display the sponsor information.
 */
export const Sponsor = ({
  name, link, image, children,
}) => {
  return <table>
  <tr/>
  <tr>
    <td align="center">
      <a href={link}>
        <img src={image} alt={name}/>
      </a><br/>
      Sponsored by <a href={link}>{name}</a>.
    </td>
  </tr>
  {children && <tr><td>{children}</td></tr>}
</table>
}

/**
 * The async component to print the source of the document.
 */
export const Source = async ({ src }) => {
  const res = await read(src)
  const e = src.split('.')
  const ext = e[e.length - 1]
  return `\`\`\`${ext}
${res}
\`\`\``
}

const PipelineBadge = ({ version = 'master' }) => {
  const r = repository.replace('gitlab:', '')
  const badge = format({
    protocol: 'https',
    host: 'gitlab.com',
    pathname: `${r}/badges/${version}/pipeline.svg`
  })
  const commits = format({
    protocol: 'https',
    host: 'gitlab.com',
    pathname: `${r}/-/commits/${version}`
  })
  return (<a href={commits}><img src={badge} alt="Pipeline Badge"/></a>)
}

export default {
  'pipeline-badge': PipelineBadge
}