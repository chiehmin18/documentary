import { join, dirname, relative, parse, sep, resolve } from 'path'
import { parse as parseUrl } from 'url'
import { writeFileSync } from 'fs'
import { c } from 'erte'
import { getLink } from '../lib'
import { EOL } from 'os'

export default function Annotate(wiki, types, gl = ({ name }) => {
  return getLink(name, 'type')
}) {
  const packageJson = require(join(process.cwd(), 'package.json'))
  const { repository } = packageJson
  let github, URL, WIKI_URL
  if (typeof repository == 'string') {
    const [portal, name] = repository.split(':')
    if (name && portal != 'github') {
      switch (portal) {
      case 'gitlab':
        URL = `https://gitlab.com/${name}`
        WIKI_URL = `${URL}/-/wikis/`
        break
      default:
        throw new Error(`${portal} is not supported as a repository in package.json.`)
      }
    } else if (name) github = name
    else github = repository
  } else {
    const { url } = repository
    const { host, pathname } = parseUrl(url)
    if (host != 'github.com') throw new Error('Only GitHub is supported for repositories in package.json.')
    github = pathname.replace(/\.git$/, '').replace(/^\//, '')
  }
  // github
  if (!URL) {
    URL = `https://github.com/${github}`
    WIKI_URL = `${URL}/wiki/`
  }
  let t = null
  const f = types.filter(({ import: i, external }) => !i && !external)
  if (wiki) {
    t = f.reduce((acc, type) => {
      const { name, appearsIn, description, originalNs } = type
      const [ai] = appearsIn.map((file) => {
        let rel = relative(dirname(file), file)
        const [bn] = rel.split(sep)
        const { name: n } = parse(bn)
        return n
      })
      const link = gl(type)
      const r = `${ai}#${link}`
      const rr = `${WIKI_URL}${r}`
      acc[`${originalNs}${name}`] = {
        link: rr,
        description,
      }
      return acc
    }, {})
  } else {
    t = f.reduce((acc, type) => {
      const { name, description, originalNs } = type
      const rr = `${URL}#${gl(type)}`
      acc[`${originalNs}${name}`] = {
        link: rr,
        description,
      }
      return acc
    }, {})
  }
  if (t && Object.keys(t).length) {
    let current = {}
    try {
      const r = resolve('typedefs.json')
      delete require.cache[r]
      current = require(r)
    } catch (err) { /* */}
    writeFileSync('typedefs.json', JSON.stringify({ ...current, ...t }, null, 2))

    writePackageJson(packageJson)

    Object.keys(t).forEach((tt) => {
      console.log(' - %s', tt)
    })
  }
}

/**
 * Update `package.json`.
 * @param {Object} packageJson
 * @param {string} [path]
 */
const writePackageJson = (packageJson, path = 'package.json') => {
  if (!('typedefs' in packageJson)) {
    const newPackageJson = Object.entries(packageJson).reduce((acc, [k, v]) => {
      acc[k] = v
      if (k == 'repository') {
        acc['typedefs'] = 'typedefs.json'
      }
      return acc
    }, {})
    if ('files' in packageJson && !packageJson.files.includes('typedefs.json')) {
      packageJson.files.push('typedefs.json')
    }
    writeFileSync(path, JSON.stringify(newPackageJson, null, 2) + EOL)
    console.log('Updated %s with', c('typedefs.json', 'yellow'))
  }
}